let $form = document.getElementById('formulaire');
let $input = document.getElementById('number');
let $divResultat = document.getElementById('resultat');

function factorielle(number) {
    if (number === 1 ) {
        return 1;
    } else {
        return number*factorielle(number-1);
    }
}

$form.addEventListener('submit', (e) => {
    e.preventDefault();
    const value =  parseInt($input.value);
    $form.remove();
    $divResultat.append('Factorielle de ' + value + ' : ' + factorielle(value));
});
