let $form = document.getElementById('formulaire');
let $partants = document.getElementById('partants');
let $joue = document.getElementById('joue');
let $divResultat = document.getElementById('resultat');

function factorielle(number) {
    if (number === 1 ) {
        return 1;
    } else {
        return number*factorielle(number-1);
    }
}

$form.addEventListener('submit', (e) => {
    e.preventDefault();
    const nbrPartant =  parseInt($partants.value);
    const nbrjoue =  parseInt($joue.value);
    $form.remove();
    $divResultat.append('Dans l ordre : 1 chance sur ' + factorielle(nbrPartant)/factorielle(nbrPartant - nbrjoue) + ' Dans le désordre : 1 chance sur ' + factorielle(nbrPartant)/(factorielle(nbrPartant)*factorielle(nbrPartant - nbrjoue)));
});
