let $form = document.getElementById('formulaire');
let $str = document.getElementById('str');
let $divResultat = document.getElementById('resultat');

$form.addEventListener('submit', (e) => {
    e.preventDefault();
    const strLenght =  $str.value.length;
    $form.remove();
    $divResultat.append('Longueur de la string ' + strLenght);
});
