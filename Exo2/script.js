let $form = document.getElementById('formulaire');
let $input = document.getElementById('number');
let $button = document.getElementById('formBtn');
let $divTable = document.getElementById('tableMultiplication');

$form.addEventListener('submit', (e) => {
    e.preventDefault();
    const value =  parseInt($input.value);
    $form.remove();
    $divTable.append('Table de multiplication ' + value + ' : ');
    for (let i = 1; i<=10; i++) {
        $divTable.append( i + ' x ' + value + ' = ' + (i*value) + ' | ' );
    }
});
