let $form = document.getElementById('formulaire');
let $input = document.getElementById('number');
let $button = document.getElementById('formBtn');
let values = [];

$form.addEventListener('submit', (e) => {
    e.preventDefault();
    values.push(parseInt($input.value));
    $input.value = '';
    if (values.length >= 20) {
        $input.remove();
        $button.remove();
        $form.append('Votre plus grand nombre entré : ');
        $maxNumber = Math.max( ...values );
        $form.append($maxNumber);
        $form.append(' Ce nombre était en : ');
        $form.append(values.indexOf($maxNumber)+1);
        $form.append(' position ');
    }
});
