//Exo 6
// Faire une fonction de concaténation (ajoute à la fin de la première chaîne de caractères le contenu de la deuxième chaîne de caractères.)
function concatStr(stringToConcat, newStrPart) {
    stringToConcat = stringToConcat + newStrPart
    return stringToConcat
}
// exo 7
// Ecrire un algorithme qui affiche la moyenne d’une suite d’entiers
function calcMoyenne (intArray) {
    let sommeInt = 0;
    for (const intArrayElement of intArray) {
        sommeInt = sommeInt + intArrayElement;
    }
    return sommeInt/intArray.length;
}
//exo 8
// max3
function max3(int1, int2, int3){
    if(int1>int2) {
        if(int1>int3) {
            return int1;
        }
    } else {
        if(int2>int3) {
            return int2;
        } else {
            return int3;
        }
    }
}
// min3
function min3(int1, int2, int3){
    if(int1<int2) {
        if(int1<int3) {
            return int1;
        }
    } else {
        if(int2<int3) {
            return int2;
        } else {
            return int3;
        }
    }
}
//max2
function max2(int1, int2){
    if(int1>int2) {
        return int1;
    } else {
        return int2;
    }
}
//max3 en utilisant max3 bis
function max3bis(int1, int2, int3){
    return max2(max2(int1, int2), int3);
}

// exo 9
function grantStudent(note) {
    if (note >= 10) {
        alert('Felicitations');
    } else {
        alert('Nope, recalé...');
    }
}

grantStudent(10);
